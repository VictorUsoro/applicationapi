using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace Hahn.ApplicatonProcess.May2020.Web
{
    ///
    public class Startup
    {
        ///
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        ///
        public IConfiguration Configuration { get; }

        ///
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicantDbContext>(options => options.UseInMemoryDatabase(databaseName: "ApplicantInMemoryDB"));
            services.AddControllers();

            services.AddScoped<IResponseService, ResponseService>();
            services.AddScoped<IApplicantService, ApplicantService>();

            services.AddTransient<IBaseHttpClient, BaseHttpClient>();
            services.AddHttpClient();
            services.AddRouting(r => r.SuppressCheckForUnhandledSecurityMetadata = true);

            services.AddSwaggerGen(c =>
           {
               c.SwaggerDoc("v1", new OpenApiInfo
               {
                   Version = "v1",
                   Title = "Hahn",
                   Description = "Hahn application process api documentation",
                   TermsOfService = new Uri("https://google.com"),
                   Contact = new OpenApiContact()
                   {
                       Name = "Developer's Contact",
                       Email = string.Empty,
                       Url = new Uri("https://twitter.com/VictorUdUsoro")
                   },
                   License = new OpenApiLicense
                   {
                       Name = "google",
                       Url = new Uri("https://google.com")
                   },
               });

               var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
               var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
               c.IncludeXmlComments(xmlPath);
           });
        }

        ///
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseExceptionHandler(logger);
            app.UseRouting();
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
