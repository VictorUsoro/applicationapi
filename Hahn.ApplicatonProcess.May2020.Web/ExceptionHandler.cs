using System;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

///
public static class ExceptionMiddlewareExtensions
{
    ///
    public static void UseExceptionHandler(this IApplicationBuilder app, ILoggerFactory logger)
    {
        var _logger = logger.CreateLogger("ConfigureExceptionHandler");
        app.UseExceptionHandler(appError =>
        {
            appError.Run(async context =>
            {
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Response.ContentType = "application/json";

                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                if (contextFeature != null)
                {
                    _logger.LogError($"Something went wrong as at {DateTime.Now} - {contextFeature.Error}");
                    await context.Response.WriteAsync(Utilities.SerializeJson(new ResponseService()
                        .ErroResponse<string>("An error occurred, Don't worry we are aware of it, and it will be resolved soon.")));
                }
            });
        });
    }
}
