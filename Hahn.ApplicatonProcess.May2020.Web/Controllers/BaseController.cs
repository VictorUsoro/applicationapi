using System;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

///
[ApiController]
[Route("[controller]")]
[EnableCors("HahnCoresPolicy")]
public class BaseController : Controller
{
    #region Validation Message

    ///
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        if (!ModelState.IsValid)
        {
            var message = string.Join(", ", ModelState.Values.SelectMany(x => x.Errors)
                .Select(err => err.ErrorMessage)).Replace("field", string.Empty).Replace("The", string.Empty); ;
            context.Result = Error(message);
        }
        base.OnActionExecuting(context);
    }

    #endregion

    ///
    protected IActionResult Error(string message)
    {
        return BadRequest(new ResponseService().ErroResponse<string>(message));
    }

}
