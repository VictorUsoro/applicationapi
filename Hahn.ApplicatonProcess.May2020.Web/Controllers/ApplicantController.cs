using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

///
public class ApplicantController : BaseController
{
    private readonly IApplicantService _applicantService;
    ///
    public ApplicantController(IApplicantService applicantService)
    {
        _applicantService = applicantService;
    }


    /// <summary>
    /// Creates an applicant.
    /// </summary>
    /// <remarks>
    /// Sample request:
    /// 
    ///     POST api/Applicant
    ///     {        
    ///       "name": "Victor",
    ///       "familyName": "Uduak",
    ///       "age": 28,
    ///       "hired": true,
    ///       "CountryOfOrigin": "Nigeria",
    ///       "address":"Surulere, Lagos, Nigeria",
    ///       "eMailAddress": "victorusoro@hotmail.com"        
    ///     }
    /// </remarks> 
    /// <returns>string url to get the created object</returns>
    // POST: Applicant/create
    [HttpPost("create")]
    [ProducesResponseType(type: typeof(ServiceResponse<string>), statusCode: 201)]
    [ProducesResponseType(type: typeof(ServiceResponse<string>), statusCode: 400)]
    public async Task<IActionResult> CreateAsync([FromBody] CreateApplicantModel model)
    {
        var serviceResp = await _applicantService.CreateAsync(model);
        if (serviceResp.Status) return Created(serviceResp.Data, serviceResp.Message);
        return BadRequest(serviceResp.Message);
    }


    /// <summary>
    /// Gets the list of all applicants.
    /// </summary>
    /// <returns>The list of Applicants</returns>
    // GET: Applicant/get
    [HttpGet("get")]
    [ProducesResponseType(type: typeof(ServiceResponse<List<ApplicantModel>>), statusCode: 200)]
    public async Task<IActionResult> GetAllAsync()
    {
        var serviceResp = await _applicantService.GetAllAsync();
        if (serviceResp.Status) return Ok(serviceResp);
        return BadRequest(serviceResp.Message);
    }


    /// <summary>
    /// Gets applicant by Id.
    /// </summary>
    /// <returns>The Applicant's details by Id</returns>
    // GET: Applicant/get/{id}
    [HttpGet("get/{id}")]
    [ProducesResponseType(type: typeof(ServiceResponse<ApplicantModel>), statusCode: 200)]
    public async Task<IActionResult> GetByIdAsync(int id)
    {
        var serviceResp = await _applicantService.GetByIdAsync(id);
        if (serviceResp.Status) return Ok(serviceResp);
        return BadRequest(serviceResp.Message);
    }


    /// <summary>
    ///Update applicant details.
    /// </summary>
    /// <returns>The Applicant's details after update</returns>
    // PUT: Applicant/update/
    [HttpPut("update")]
    [ProducesResponseType(type: typeof(ServiceResponse<ApplicantModel>), statusCode: 200)]
    public async Task<IActionResult> UpdateAsync([FromBody] ApplicantModel model)
    {
        var serviceResp = await _applicantService.UpdateAsync(model);
        if (serviceResp.Status) return Ok(serviceResp);
        return BadRequest(serviceResp.Message);
    }


    /// <summary>
    ///Soft delete applicant.
    /// </summary>
    /// <returns>success message</returns>
    // DELETE: Applicant/soft-delete/{id}
    [HttpDelete("soft-delete/{id}")]
    [ProducesResponseType(type: typeof(ServiceResponse<string>), statusCode: 200)]
    public async Task<IActionResult> SoftDeleteAsync(int id)
    {
        var serviceResp = await _applicantService.SoftDeleteAsync(id);
        if (serviceResp.Status) return Ok(serviceResp);
        return BadRequest(serviceResp.Message);
    }


    /// <summary>
    ///delete applicant.
    /// </summary>
    /// <returns>success message</returns>
    // DELETE: Applicant/delete/{id}
    [HttpDelete("delete/{id}")]
    [ProducesResponseType(type: typeof(ServiceResponse<string>), statusCode: 200)]
    public async Task<IActionResult> DeleteAsync(int id)
    {
        var serviceResp = await _applicantService.RemoveAsync(id);
        if (serviceResp.Status) return Ok(serviceResp);
        return BadRequest(serviceResp.Message);
    }
}