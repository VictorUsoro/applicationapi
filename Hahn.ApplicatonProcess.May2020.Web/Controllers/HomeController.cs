using Microsoft.AspNetCore.Mvc;

///
[Route("/")]
[ApiExplorerSettings(IgnoreApi = true)]
public class HomeController : ControllerBase
{
    ///
    public IActionResult GetAction()
    {
        return Ok("Hahn Application Process API. visit /swagger for api documentation");
    }
}
