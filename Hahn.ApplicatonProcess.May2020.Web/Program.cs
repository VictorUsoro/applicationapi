using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Hahn.ApplicatonProcess.May2020.Web
{
    ///
    public class Program
    {
        ///
        public static void Main(string[] args)
        {
            var appHost = CreateHostBuilder(args).Build();

            using (var scope = appHost.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<ApplicantDbContext>();
                DataGenerator.Initialize(services);
            }

            appHost.Run();
        }

        ///
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
