using System.Collections.Generic;
using Newtonsoft.Json;

public class CountryResp
{
    [JsonProperty("name")]
    public string Name { get; set; }
}
