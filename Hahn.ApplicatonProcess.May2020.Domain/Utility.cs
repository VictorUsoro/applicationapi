using System.IO;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using Newtonsoft.Json;

public static class Utilities
{
    public static bool IsValidEmail(this string email)
    {
        try
        {
            var addr = new MailAddress(email);
            if (addr.Address != email) return false;

            var split = email.Split('@')[1];
            if (!split.Contains('.')) return false;

            return true;
        }
        catch
        {
            return false;
        }
    }

    public static T DeserializeJson<T>(string input)
    {
        return JsonConvert.DeserializeObject<T>(input);
    }

    public static string SerializeJson(object input)
    {
        return JsonConvert.SerializeObject(input);
    }

    //I was supposed to use the new system.text deserializer built by asp.net core team but its not supported in netstandards2.1 
    public static async Task<T> DeserializeRequestAsync<T>(HttpResponseMessage response)
    {
        var contentStream = await response.Content.ReadAsStreamAsync();
        // var result = await JsonSerializer.DeserializeAsync<T>(contentStream, Options);
        // return result;
        using StreamReader reader = new StreamReader(contentStream);
        using JsonTextReader jsonReader = new JsonTextReader(reader);
        JsonSerializer ser = new JsonSerializer();
        return ser.Deserialize<T>(jsonReader);
    }
}