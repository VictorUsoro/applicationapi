﻿using System;
public interface IEntity
{
    int Id { get; set; }
    DateTime CreatedDate { get; set; }
    DateTime? ModifiedDate { get; set; }
    string CreatedBy { get; set; }
    string ModifiedBy { get; set; }
    DateTime? Deleted { get; set; }
    bool IsDeleted { get; set; }
}

