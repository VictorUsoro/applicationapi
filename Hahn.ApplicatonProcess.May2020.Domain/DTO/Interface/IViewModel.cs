using System;
public interface IViewModel
{
    int Id { get; set; }
    DateTime CreatedDate { get; set; }
}
