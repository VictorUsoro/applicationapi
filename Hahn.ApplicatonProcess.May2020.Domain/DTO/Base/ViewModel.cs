using System;

public abstract class ViewModel : IViewModel
{
    public int Id { get; set; }
    public DateTime CreatedDate { get; set; }
}
