using System.ComponentModel.DataAnnotations;

public class Applicant : Entity
{
    public string Name { get; set; }
    public string FamilyName { get; set; }
    public string Address { get; set; }
    public string CountryOfOrigin { get; set; }
    public string EMailAddress { get; set; }
    public int Age { get; set; }
    public bool Hired { get; set; }
}

public class ApplicantModel : ViewModel
{
    [Required(ErrorMessage = "Name is required")]
    [MinLength(5, ErrorMessage = "Name should not be less than 5 characters")]
    [MaxLength(99, ErrorMessage = "Name should not be more than 99 characters")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Family Name is required")]
    [MinLength(5, ErrorMessage = "Family Name shouldnot be less than 5 characters")]
    [MaxLength(99, ErrorMessage = "Family Name should not be more than 99 characters")]
    public string FamilyName { get; set; }

    [Required(ErrorMessage = "Address is required")]
    [MinLength(10, ErrorMessage = "Adress should not be less than 10 characters")]
    [MaxLength(99, ErrorMessage = "Adress should not be more than 99 characters")]
    public string Address { get; set; }

    [Required(ErrorMessage = "Country of origin is required")]
    public string CountryOfOrigin { get; set; }

    [Required(ErrorMessage = "Email Adress is required")]
    public string EMailAddress { get; set; }
    public int Age { get; set; }
    public bool Hired { get; set; }
}

public class CreateApplicantModel
{
    [Required(ErrorMessage = "Name is required")]
    [MinLength(5, ErrorMessage = "Name should not be less than 5 characters")]
    [MaxLength(99, ErrorMessage = "Name should not be more than 99 characters")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Family Name is required")]
    [MinLength(5, ErrorMessage = "Family Name shouldnot be less than 5 characters")]
    [MaxLength(99, ErrorMessage = "Family Name should not be more than 99 characters")]
    public string FamilyName { get; set; }

    [Required(ErrorMessage = "Address is required")]
    [MinLength(10, ErrorMessage = "Adress should not be less than 10 characters")]
    [MaxLength(99, ErrorMessage = "Adress should not be more than 99 characters")]
    public string Address { get; set; }

    [Required(ErrorMessage = "Country of origin is required")]
    public string CountryOfOrigin { get; set; }

    [Required(ErrorMessage = "Email Adress is required")]
    public string EMailAddress { get; set; }

    [Range(20, 60, ErrorMessage = "Applicant should be between the age of 20 and 60")]
    public int Age { get; set; }
    public bool Hired { get; set; }
}