using System;

public interface IResponseService
{
    ServiceResponse<T> ErroResponse<T>(string message) where T : class;
    ServiceResponse<T> SuccessResponse<T>(string message, T data) where T : class;
    ServiceResponse<T> ExceptionResponse<T>(Exception exception) where T : class;
}

public class ResponseService : IResponseService
{
    public ServiceResponse<T> ErroResponse<T>(string message)
        where T : class => new ServiceResponse<T>
        {
            Message = message,
            Status = false,
            Data = null
        };

    public ServiceResponse<T> ExceptionResponse<T>(Exception exception)
        where T : class => new ServiceResponse<T>
        {
            Message = exception.Message,
            Status = false,
            Data = null
        };

    public ServiceResponse<T> SuccessResponse<T>(string message, T data)
        where T : class => new ServiceResponse<T>
        {
            Message = message,
            Status = true,
            Data = data
        };
}