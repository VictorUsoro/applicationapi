using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

public interface IApplicantService
{
    Task<ServiceResponse<string>> CreateAsync(CreateApplicantModel model);
    Task<ServiceResponse<List<ApplicantModel>>> GetAllAsync();
    Task<ServiceResponse<ApplicantModel>> GetByIdAsync(int Id);
    Task<ServiceResponse<ApplicantModel>> UpdateAsync(ApplicantModel model);
    Task<ServiceResponse<string>> SoftDeleteAsync(int id);
    Task<ServiceResponse<string>> RemoveAsync(int id);
}

public class ApplicantService : IApplicantService
{
    private readonly IResponseService _responseService;
    private readonly ApplicantDbContext _dbContext;
    private readonly IConfiguration _config;
    private readonly IBaseHttpClient _httpClient;
    public ApplicantService(IResponseService responseService, IBaseHttpClient httpClient,
        ApplicantDbContext dbContext, IConfiguration config)
    {
        _responseService = responseService;
        _dbContext = dbContext;
        _config = config;
        _httpClient = httpClient;
    }

    public async Task<ServiceResponse<string>> CreateAsync(CreateApplicantModel model)
    {
        if (!model.EMailAddress.IsValidEmail()) return _responseService.ErroResponse<string>("Enter a Valid email address");

        var baseURL = _config["ContriesJsonURL"];
        var response = await _httpClient.GetAsync<List<CountryResp>>(baseUrl: baseURL, url: model.CountryOfOrigin.Trim());
        if (!response.Any()) return _responseService.ErroResponse<string>("Invalid country name");

        var applicant = new Applicant
        {
            Name = model.Name,
            EMailAddress = model.EMailAddress,
            Address = model.Address,
            FamilyName = model.FamilyName,
            Age = model.Age,
            CountryOfOrigin = model.CountryOfOrigin,
            Hired = model.Hired,
            CreatedDate = DateTime.Now
        };

        await _dbContext.AddAsync(applicant);
        await _dbContext.SaveChangesAsync();

        var url = $"{_config["Environment"]}applicant/get/{applicant.Id}";
        return _responseService.SuccessResponse("Applicat created successfully..", url);
    }

    public async Task<ServiceResponse<List<ApplicantModel>>> GetAllAsync()
    {
        var applicatsQuery = _dbContext.Applicants.AsNoTracking().Where(x => !x.IsDeleted);
        var applicants = await applicatsQuery.Select(x => new ApplicantModel
        {
            Id = x.Id,
            Address = x.Address,
            Name = x.Name,
            Age = x.Age,
            FamilyName = x.FamilyName,
            CreatedDate = x.CreatedDate,
            Hired = x.Hired,
            EMailAddress = x.EMailAddress
        }).ToListAsync();
        return _responseService.SuccessResponse("Applicat retrieved successfully..", applicants);
    }

    public async Task<ServiceResponse<ApplicantModel>> GetByIdAsync(int Id)
    {
        var applicatsQuery = _dbContext.Applicants.AsNoTracking().Where(x => !x.IsDeleted && x.Id == Id);
        if (!applicatsQuery.Any()) return _responseService.ErroResponse<ApplicantModel>("Invalid applicant Id");

        var applicant = await applicatsQuery.Select(x => new ApplicantModel
        {
            Id = x.Id,
            Address = x.Address,
            Name = x.Name,
            Age = x.Age,
            FamilyName = x.FamilyName,
            CreatedDate = x.CreatedDate,
            Hired = x.Hired,
            EMailAddress = x.EMailAddress
        }).FirstOrDefaultAsync();

        return _responseService.SuccessResponse("Applicant details retrieved successfully..", applicant);
    }

    public async Task<ServiceResponse<ApplicantModel>> UpdateAsync(ApplicantModel model)
    {
        var applicant = await _dbContext.Applicants.FirstOrDefaultAsync(x => x.Id == model.Id && !x.IsDeleted);
        if (applicant == null) return _responseService.ErroResponse<ApplicantModel>("Invalid request");

        applicant.Name = model.Name;
        applicant.EMailAddress = model.EMailAddress;
        applicant.Address = model.Address;
        applicant.CountryOfOrigin = model.CountryOfOrigin;
        applicant.Age = model.Age;
        applicant.Hired = model.Hired;
        applicant.FamilyName = model.FamilyName;

        _dbContext.Update(applicant);
        await _dbContext.SaveChangesAsync();

        return _responseService.SuccessResponse("Applicant details updated successfully..", model);
    }

    public async Task<ServiceResponse<string>> SoftDeleteAsync(int id)
    {
        var applicant = await _dbContext.Applicants.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);
        if (applicant == null) return _responseService.ErroResponse<string>("Invalid request");

        applicant.IsDeleted = true;
        applicant.Deleted = DateTime.Now;

        _dbContext.Update(applicant);
        await _dbContext.SaveChangesAsync();

        return _responseService.SuccessResponse("Applicant details updated successfully..", "success");
    }

    public async Task<ServiceResponse<string>> RemoveAsync(int id)
    {
        var applicant = await _dbContext.Applicants.FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);
        if (applicant == null) return _responseService.ErroResponse<string>("Invalid request");

        _dbContext.Remove(applicant);
        await _dbContext.SaveChangesAsync();

        return _responseService.SuccessResponse("Applicant details removed successfully..", "success");
    }
}
