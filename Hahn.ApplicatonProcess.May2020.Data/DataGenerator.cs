using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

public class DataGenerator
{
    public static void Initialize(IServiceProvider serviceProvider)
    {
        var serviceContext = serviceProvider.GetRequiredService<DbContextOptions<ApplicantDbContext>>();
        using var context = new ApplicantDbContext(serviceContext);
        if (context.Applicants.Any())
        {
            return;
        }

        context.Applicants.AddRange(
            new Applicant
            {
                Id = 1,
                Name = "Victor",
                FamilyName = "Usoro",
                Address = "Lagos",
                CountryOfOrigin = "Nigeria",
                EMailAddress = "viclov91@gmail.com",
                Age = 28,
                Hired = true,
                CreatedDate = DateTime.Now
            });

        context.SaveChanges();
    }
}