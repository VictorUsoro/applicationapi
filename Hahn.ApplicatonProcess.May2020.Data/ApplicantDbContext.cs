using Microsoft.EntityFrameworkCore;

public class ApplicantDbContext : DbContext
{
    public ApplicantDbContext(DbContextOptions<ApplicantDbContext> options)
        : base(options) { }

    public DbSet<Applicant> Applicants { get; set; }
}