using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

public class BaseHttpClient : IBaseHttpClient
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ILogger<BaseHttpClient> _logger;
    public BaseHttpClient(IHttpClientFactory httpClientFactory, ILogger<BaseHttpClient> logger)
    {
        _httpClientFactory = httpClientFactory;
        _logger = logger;
    }

    public virtual async Task<T> GetAsync<T>(string baseUrl, string url, string token = null, string apikey = null)
    {
        T returnValue;

        var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(apikey))
            client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        var _url = $"{baseUrl}{url}";
        var request = new HttpRequestMessage(HttpMethod.Get, _url);
        var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

        //I dont use this to read my json responses, am doing this because am contrained to using netstandards2.1 and 
        //there is no support for System.Json.Text serializer from .net core team

        var stringHttpResponse = await httpResponse.Content.ReadAsStringAsync();
        returnValue = Utilities.DeserializeJson<T>(stringHttpResponse);
        return returnValue;

        //this is what I use.. I thried to fit in this one to newtonsoft to see how it works, it still did not work out as expected
        // returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
        // return returnValue;

    }

    public async Task<T> PostAsync<T>(string baseUrl, object postdata, string url, string token = null, string apikey = null)
    {
        T returnValue;

        var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(apikey))
            client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        var _url = $"{baseUrl}{url}";
        var request = new HttpRequestMessage(HttpMethod.Post, _url);
        var jsonContent = Utilities.SerializeJson(postdata);
        _logger.LogInformation(jsonContent);
        request.Content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
        var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

        returnValue = await Utilities.DeserializeRequestAsync<T>(httpResponse);
        return returnValue;
    }

    public async Task<string> PostAsync(string baseUrl, object postdata, string url, string token = null, string apikey = null)
    {
        var client = _httpClientFactory.CreateClient();
        if (!string.IsNullOrEmpty(apikey))
            client.DefaultRequestHeaders.Add("api-key", apikey);
        if (!string.IsNullOrEmpty(token))
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        var _url = $"{baseUrl}{url}";
        var request = new HttpRequestMessage(HttpMethod.Post, _url);
        var jsonContent = Utilities.SerializeJson(postdata);
        request.Content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
        var httpResponse = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

        var returnValue = await Utilities.DeserializeRequestAsync<string>(httpResponse);
        return returnValue;
    }
}
